#!/bin/bash

# Get the list of nodes and their names
node_info=$(kubectl get nodes --no-headers | awk '{print $1}')

# Loop through each node
while IFS= read -r node_name; do
    # Check if the node name contains "worker"
    if [[ "$node_name" == *"worker"* ]]; then
        # Label the node as a worker node
        kubectl label nodes "$node_name" node-role.kubernetes.io/worker="$node_name" --overwrite
        echo "Node $node_name labeled as a worker node."
    # Check if the node name contains "controlnode"
    elif [[ "$node_name" == *"controlnode"* ]]; then
        # Label the node as a control node
        kubectl label nodes "$node_name" node-role.kubernetes.io/control-plane="$node_name" --overwrite
        echo "Node $node_name labeled as a control node."
    else
        # Skip labeling for nodes with unrecognized names
        echo "Node $node_name does not have a recognized role. Skipping labeling."
    fi
done <<< "$node_info"
