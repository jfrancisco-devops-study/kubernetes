#!/bin/bash

# Define common functions
function execute_ssh_command() {
    local hostname=$1
    local command=$2
    ssh k8admin@"$hostname" "$command"
}

# Update package repositories and install necessary packages
apt update && apt upgrade -y
apt install -y jq sshpass apt-transport-https ca-certificates curl gnupg
apt autoremove -y

# Add the user k8admin and grant sudo privileges without password
useradd -m -s /bin/bash k8admin
echo 'k8admin:k8admin' | chpasswd
echo 'k8admin ALL=(ALL) NOPASSWD: ALL' | sudo EDITOR='tee -a' visudo

# Configure SSH to disable strict host key checking
mkdir -p ~/.ssh && touch ~/.ssh/config
sed -i '/^#.*StrictHostKeyChecking/s/^#//' ~/.ssh/config
echo 'StrictHostKeyChecking no' >> ~/.ssh/config

# Define server list
servers=("k8controlnode1" "k8workernode1" "k8workernode2" "k8workernode3" "k8workernode4" "k8workernode5" "k8workernode6")

# Display server list and prompt user to select a server
echo "Server list:"
for ((i = 0; i < ${#servers[@]}; i++)); do
    echo "$((i+1)). ${servers[i]}"
done
read -p "Enter the number corresponding to the server: " server_choice

# Assign last octet based on user's selection
case $server_choice in
    [1-9]) last_octet=$((99 + server_choice)); hostname=${servers[server_choice-1]} ;;
    *) echo "Invalid choice"; exit 1 ;;
esac

# Get the first 3 octets of the IP address of ens3
ip_addr=$(ip addr show ens3 | awk '/inet / {print $2}' | cut -d '/' -f 1 | cut -d '.' -f 1-3)

# Get the default route
default_route=$(ip route | awk '/default/ {print $3}')

# Update netplan yaml
cat <<EOF > /etc/netplan/01-netcfg.yaml
network:
 version: 2
 renderer: networkd
 ethernets:
   ens3:
     addresses:
       - ${ip_addr}.${last_octet}/24
     routes:
       - to: 0.0.0.0/0
         via: $default_route
         on-link: true
     nameservers:
         addresses: [8.8.8.8, 4.2.2.2]
EOF

# Change hostname
echo "$hostname" > /etc/hostname
hostnamectl set-hostname "$hostname" 

# Add servers to /etc/hosts file
cat <<EOF>> /etc/hosts
# LAB SETUP
127.0.0.1 localhost
${ip_addr}.100 k8controlnode1
${ip_addr}.101 k8workernode1
${ip_addr}.102 k8workernode2
${ip_addr}.103 k8workernode3
${ip_addr}.104 k8workernode4
${ip_addr}.105 k8workernode5
${ip_addr}.106 k8workernode6
EOF

# Apply netplan configuration
chmod 600 /etc/netplan/01-netcfg.yaml
netplan apply

# Set hostname
hostnamectl set-hostname "$hostname"
# Configure Kubernetes
echo "overlay" | sudo tee -a /etc/modules-load.d/containerd.conf
echo "br_netfilter" | sudo tee -a /etc/modules-load.d/containerd.conf
modprobe overlay
modprobe br_netfilter

# Configure sysctl
cat <<EOF | sudo tee -a /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
sysctl --system

# Install containerd
latest_containerd_version=$(curl -s https://api.github.com/repos/containerd/containerd/releases/latest | jq -r '.tag_name' | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")
wget "https://github.com/containerd/containerd/releases/download/v$latest_containerd_version/containerd-$latest_containerd_version-linux-amd64.tar.gz" -P /tmp/
tar -C /usr/local -xzvf /tmp/containerd-$latest_containerd_version-linux-amd64.tar.gz
wget "https://raw.githubusercontent.com/containerd/containerd/main/containerd.service" -P /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now containerd

# Install runc
latest_runc_version=$(curl -s https://api.github.com/repos/opencontainers/runc/releases/latest | jq -r '.tag_name' | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")
wget "https://github.com/opencontainers/runc/releases/download/v$latest_runc_version/runc.amd64" -P /tmp/
install -m 755 /tmp/runc.amd64 /usr/local/sbin/runc

# Install CNI plugins
latest_cni_version=$(curl -s https://api.github.com/repos/containernetworking/plugins/releases/latest | jq -r '.tag_name' | grep -oE "[0-9]+\.[0-9]+\.[0-9]+")
wget "https://github.com/containernetworking/plugins/releases/download/v$latest_cni_version/cni-plugins-linux-amd64-v$latest_cni_version.tgz" -P /tmp/
mkdir -p /opt/cni/bin
tar -C /opt/cni/bin -xzvf /tmp/cni-plugins-linux-amd64-v$latest_cni_version.tgz

# Configure containerd
mkdir -p /etc/containerd
containerd config default | tee /etc/containerd/config.toml
sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
systemctl restart containerd

# Disable swap
swapoff -a
sed -i '/swap/s/^/#/' /etc/fstab

# Install Kubernetes components
mkdir -p -m 755 /etc/apt/keyrings
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt-get update

# Get the latest version of kubeadm
latest_kubeadm_version=$(apt-cache madison kubeadm | awk '{print $3}' | head -n 1)

# Get the latest version of kubelet
latest_kubelet_version=$(apt-cache madison kubelet | awk '{print $3}' | head -n 1)

# Get the latest version of kubectl
latest_kubectl_version=$(apt-cache madison kubectl | awk '{print $3}' | head -n 1)

# Install the latest versions and hold them to prevent automatic upgrades
apt-get install -y kubelet="$latest_kubelet_version" kubeadm="$latest_kubeadm_version" kubectl="$latest_kubectl_version"
apt-mark hold kubelet kubeadm kubectl

# Initialize Kubernetes cluster on control nodes
if [[ "$hostname" == "k8controlnode1" ]]; then
    # Get the latest version of kubeadm
    latest_kubeadm_version=$(apt-cache madison kubeadm | awk '{print $3}' | awk -F'-' '{print $1}' | head -n 1)
    # Pull required images beforehand
    kubeadm config images pull --kubernetes-version="$latest_kubeadm_version"
    
    # Initialize the cluster on the first control node
    kubeadm init --pod-network-cidr 10.110.0.0/16 --kubernetes-version "$latest_kubeadm_version" --node-name "$hostname"
    export KUBECONFIG=/etc/kubernetes/admin.conf

    # Add Calico CNI
    kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.27.2/manifests/tigera-operator.yaml
    wget https://raw.githubusercontent.com/projectcalico/calico/v3.27.2/manifests/custom-resources.yaml
    sed -i 's/cidr: 192\.168\.0\.0\/16/cidr: 10.110.0.0\/16/' custom-resources.yaml
    kubectl apply -f custom-resources.yaml

    # Copy kubeconfig to k8admin user's home directory
    sudo mkdir -p ~k8admin/.kube
    sudo cp -i /etc/kubernetes/admin.conf ~k8admin/.kube/config
    sudo chown -R k8admin:k8admin ~k8admin/.kube
        
    # Copy kubeconfig to root user's home directory
    sudo mkdir -p /root/.kube
    sudo cp -i /etc/kubernetes/admin.conf /root/.kube/config
    sudo chown root:root /root/.kube/config
fi

# Check if the hostname contains "workernode"
if [[ $hostname == *"workernode"* ]]; then
    echo "Executing on a worker node: $hostname"
    ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa -N ""
    password="k8admin"
    sshpass -p "$password" ssh-copy-id k8admin@k8controlnode1
    # SSH into k8controlnode1 to get joincluster value and store it in the variable
    joincluster=$(execute_ssh_command "k8controlnode1" "kubeadm token create --print-join-command")

    # Now you can use the $joincluster variable as needed
    echo "Join command: $joincluster"
    eval "$joincluster"
else
    echo "Not a worker node: $hostname"
fi
