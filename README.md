# Kubernetes

# Kubernetes Cluster Setup Script

This script automates the setup of a Kubernetes cluster with control nodes and worker nodes.

## Prerequisites

- Ubuntu operating system
- Root access to the servers
- Internet connectivity

## Instructions

1. Download the script to your server.
2. Make the script executable with the command `chmod +x setup_kubernetes_cluster.sh`.
3. Execute the script with `./setup_kubernetes_cluster.sh`.
4. Follow the prompts to select the server roles and provide necessary inputs.

## Script Overview

- **Update Packages:** Updates package repositories and installs necessary packages.
- **Add User:** Adds the user `k8admin` and grants sudo privileges without a password.
- **Configure SSH:** Disables strict host key checking for SSH connections.
- **Network Configuration:** Configures network settings and updates the hostname and hosts file.
- **Kubernetes Configuration:** Configures Kubernetes dependencies including containerd and CNI plugins.
- **Install Kubernetes Components:** Installs the latest versions of `kubelet`, `kubeadm`, and `kubectl`.
- **Initialize Kubernetes Cluster:** Initializes the Kubernetes cluster on control nodes.
- **SSH Key Management:** Generates SSH keys and copies them to the control node for worker node setup.

## Notes

- This script assumes Ubuntu as the operating system and may require adjustments for other distributions.
- Ensure to review the script and adapt it to your specific environment before execution.
